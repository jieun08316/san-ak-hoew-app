import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          '산악회(등산 사랑회 소개)',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        elevation: 0.0,
        leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              print('메뉴버튼을 클릭했습니다.');
            }),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Text('산악회소개',
                  style: TextStyle(
                      fontFamily: 'PretendardR',
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54)),
            ),
            Container(
              child: Image.asset(
                'assets/sanak.jpeg',
                width: 1000,
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Container(
                    child: Text(
                      '등산사랑 산악회',
                      style: TextStyle(
                          fontFamily: 'Pretendard-Regular',
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.black54),
                    ),
                  ),
                  Container(
                    child: Column(children: [
                      Text('회비 : 10,000원',
                          style: TextStyle(
                            fontFamily: 'Pretendard-Regular',
                            fontSize: 15,
                              color: Colors.black54
                          )),
                      Text('매주 토요일 새벽 6시 출발',
                          style: TextStyle(
                            fontFamily: 'Pretendard-Regular',
                            fontSize: 15,
                              color: Colors.black54
                          )),
                    ]),
                  )
                ],
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    padding: EdgeInsets.all(30),
                    child: Text('멤버',
                        style: TextStyle(
                            backgroundColor: Colors.pinkAccent,
                            color: Colors.white,
                            fontFamily: 'Pretendard-Regular',
                            fontSize: 30,
                            fontWeight: FontWeight.bold)
                    )
                ),
                Container(
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Image.asset('assets/human.jpg', width: 200),
                          Text(' 회장 : 디트 ',
                              style: TextStyle(
                                backgroundColor: Colors.redAccent,
                                fontFamily: 'Pretendard-Regular',
                                fontSize: 15,
                                  color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ))
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset(
                            'assets/human2.gif',
                            width: 205,
                          ),
                          Text(' 부회장 : 바드 ',
                              style: TextStyle(
                                backgroundColor: Colors.redAccent,
                                fontFamily: 'Pretendard-Regular',
                                fontSize: 15,
                                  color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ))
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
